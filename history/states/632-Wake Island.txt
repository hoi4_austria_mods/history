
state={
	id=632
	name="STATE_632"
	manpower=9500
	state_category = large_city
	resources={
		aluminium=100
		oil=100
		rubber=100
		steel=100
		tungsten=100
		chromium=100
	}
	history={
		owner = AUS
		buildings = {
			infrastructure = 7
			13047 = {
				naval_base = 7
			}
		}
		victory_points = {
			13047 1
		}

	}
	provinces={
		13047
	}
}
