state={
	id=430
	name="STATE_430"
	resources={
		aluminium=100
		oil=100
		rubber=100
		steel=100
		tungsten=100
		chromium=100
	}
	history={
		owner = AUS
		victory_points = {
			11929 1
		}
		buildings = {
			infrastructure = 8
			industrial_complex = 2
			11968 = {
				naval_base = 8

			}

		}

	}

	provinces={
		2843 4162 4616 7078 7124 7179 7662 9935 9991 9993 10035 10389 10416 10443 11911 11929 11968 12010 12423 13208
	}
	manpower=91973300
	buildings_max_level_factor=1.000
	state_category=large_city
}
