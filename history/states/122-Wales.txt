state= {
	id=122
	name="STATE_122"
	manpower = 2309230

	state_category = large_city

	resources={
		aluminium=100
		oil=100
		rubber=100
		steel=100
		tungsten=100
		chromium=100
	}

	history=
	{
		owner = ENG
		victory_points = { 377 5 }
		buildings = {
			infrastructure = 5
			industrial_complex = 1
			377 = {
				naval_base = 3
			}
		}
		add_core_of = ENG
		add_core_of = WLS
	}

	provinces=
	{
253 311 377 407 3274 6363 9364 11361 	}
}
