state={
	id=709
	name="STATE_709"
	manpower = 926703
	resources={
		aluminium=100
		oil=100
		rubber=100
		steel=100
		tungsten=100
		chromium=100
	}
	state_category = large_city

	history=
	{
		owner = AUS
		buildings = {
			infrastructure = 7
			2188 = {
				naval_base = 5
			}
		}
	}

	provinces={
		2188
	}
}
