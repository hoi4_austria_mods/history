﻿division_template = {
	name = "Panzer-Division"

	division_names_group = AUS_Arm_01

	regiments = {
		modern_armor = { x = 0 y = 0 }
		modern_armor = { x = 0 y = 1 }
        modern_armor = { x = 1 y = 0 }
		modern_armor = { x = 1 y = 1 }
		modern_armor = { x = 2 y = 0 }
		modern_armor = { x = 2 y = 1 }
        modern_armor = { x = 3 y = 0 }
		modern_armor = { x = 3 y = 1 }
		motorized = { x = 2 y = 0 }
        motorized = { x = 2 y = 1 }
	}
	support = {
        recon = { x = 0 y = 0 }
        engineer = { x = 0 y = 1 }
        artillery = { x = 0 y = 2 }
	}
}
division_template = {
	name = "Infanterie-Division (mot.)"

	division_names_group = AUS_MOT_02

	regiments = {
		motorized = { x = 0 y = 0 }
	    motorized = { x = 0 y = 1 }
		motorized = { x = 0 y = 2 }

		motorized = { x = 1 y = 0 }
		motorized = { x = 1 y = 1 }
		motorized = { x = 1 y = 2 }

		motorized = { x = 2 y = 0 }
	    motorized = { x = 2 y = 1 }
		motorized = { x = 2 y = 2 }
	}
	support = {
        recon = { x = 0 y = 0 }
	 	engineer = { x = 0 y = 1 }
        artillery = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Infanterie Division"
	division_names_group = AUS_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
		infantry = { x = 3 y = 0 }
	}
	support = {
		engineer = { x = 0 y = 0 }   # Pioneer Bn
        artillery = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Alpenjäger Division"
	division_names_group = AUS_MNT_01

	regiments = {
		mountaineers = { x = 0 y = 0 }
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 0 y = 2 }
		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }
		mountaineers = { x = 1 y = 2 }
		mountaineers = { x = 2 y = 0 }
		mountaineers = { x = 2 y = 1 }
	}
	support = {
		engineer = { x = 0 y = 0 }   # Pioneer Bn
        artillery = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Schnelle Division"  		# Schnelle-Division (cavalry)
	division_names_group = AUS_CAV_01

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
	}
	support = {
		recon = { x = 0 y = 0 }      # Light tank (tankette) bn
	}
}

units = {
	# Panzertruppenkommando
	division= {
		division_name = {
				is_name_ordered = yes
				name_order = 27
		}
		location = 9650
		division_template = "Panzer-Division"
		start_experience_factor = 2.3
		force_equipment_variants = { modern_armor = { owner = "AUS" } }
	}
	division= {
		division_name = {
				is_name_ordered = yes
				name_order = 28
		}
		location = 9650
		division_template = "Panzer-Division"
		start_experience_factor = 2.3
		force_equipment_variants = { modern_armor = { owner = "AUS" } }
	}
	division= {
		division_name = {
				is_name_ordered = yes
				name_order = 29
		}
		location = 9650
		division_template = "Panzer-Division"
		start_experience_factor = 2.3
		force_equipment_variants = { modern_armor = { owner = "AUS" } }
	}
	division= {
		division_name = {
				is_name_ordered = yes
				name_order = 30
		}
		location = 9650
		division_template = "Panzer-Division"
		start_experience_factor = 2.3
		force_equipment_variants = { modern_armor = { owner = "AUS" } }
	}
	division= {
		division_name = {
				is_name_ordered = yes
				name_order = 31
		}
		location = 9650
		division_template = "Panzer-Division"
		start_experience_factor = 2.3
		force_equipment_variants = { modern_armor = { owner = "AUS" } }
	}
	division= {
		division_name = {
				is_name_ordered = yes
				name_order = 32
		}
		location = 9650
		division_template = "Panzer-Division"
		start_experience_factor = 2.3
		force_equipment_variants = { modern_armor = { owner = "AUS" } }
	}


	##### Österreichisches Bundesheer #####
	# I Korps
	division= {
		name = "Schnelle Division"
		location = 11666  # Vienna
		division_template = "Schnelle Division"
		start_experience_factor = 2.1
		start_equipment_factor = 2.3

	}
	division= {	# "1. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 1
		}
		location = 11666  # Vienna
		division_template = "Infanterie Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}
	division= {	# "2. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 2
		}
		location = 11666  # Vienna
		division_template = "Infanterie Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}
	division= {	# "3. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 3
		}
		location = 11651 # St Poelten
		division_template = "Infanterie Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}

	# II Korps
	division= {	# "4. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 4
		}
		location = 9665 # Linz
		division_template = "Infanterie Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}
	division= {	# "5. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 5
		}
		location = 9648  # Graz
		division_template = "Alpenjäger Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}
	division= {	# "6. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 6
		}
		location = 673  # Innsbruck
		division_template = "Alpenjäger Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}
	division= {	# "7. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 7
		}
		location = 688  # Salzburg
		division_template = "Alpenjäger Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}

	division= {	# "8. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 8
		}
		location = 673  # Innsbruck
		division_template = "Alpenjäger Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}

	division= {	# "9. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 9
		}
		location = 673  # Innsbruck
		division_template = "Alpenjäger Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}
	division= {	# "10. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 10
		}
		location = 688  # Salzburg
		division_template = "Alpenjäger Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}

	division= {	# "11. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 11
		}
		location = 688  # Salzburg
		division_template = "Alpenjäger Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}

	division= {	# "12. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 12
		}
		location = 673  # Innsbruck
		division_template = "Alpenjäger Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}
	division= {	# "13. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 13
		}
		location = 688  # Salzburg
		division_template = "Alpenjäger Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}

	division= {	# "14. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 14
		}
		location = 688  # Salzburg
		division_template = "Alpenjäger Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}

	division= {	# "15. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 15
		}
		location = 673  # Innsbruck
		division_template = "Alpenjäger Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}
	division= {	# "16. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 16
		}
		location = 688  # Salzburg
		division_template = "Alpenjäger Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}

	division= {	# "17. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 17
		}
		location = 688  # Salzburg
		division_template = "Alpenjäger Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}

	division= {	# "18. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 18
		}
		location = 673  # Innsbruck
		division_template = "Alpenjäger Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}
	division= {	# "19. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 19
		}
		location = 688  # Salzburg
		division_template = "Alpenjäger Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}

	division= {	# "20. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 20
		}
		location = 688  # Salzburg
		division_template = "Alpenjäger Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}

	division= {	# "21. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 21
		}
		location = 11651 # St Poelten
		division_template = "Infanterie Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}

	division= {	# "22. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 22
		}
		location = 11651 # St Poelten
		division_template = "Infanterie Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}

	division= {	# "23. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 23
		}
		location = 11651 # St Poelten
		division_template = "Infanterie Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}

	division= {	# "24. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 24
		}
		location = 11651 # St Poelten
		division_template = "Infanterie Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}

	division= {	# "25. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 25
		}
		location = 11651 # St Poelten
		division_template = "Infanterie Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}

	division= {	# "26. Division"
		division_name = {
				is_name_ordered = yes
				name_order = 26
		}
		location = 11651 # St Poelten
		division_template = "Infanterie Division"
		start_experience_factor = 3.9
		start_equipment_factor = 2.3

	}
}

air_wings = {
	### Luftstreitkräfte -- Vienna (full air force, consolidated), CR.32 aircraft
	4 = {
		fighter_equipment_1 =  {
			owner = "AUS"
			creator = "ITA"
			amount = 5000
		}
	}
}

### Starting Production ###
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = "AUS"
		}
		requested_factories = 1
		progress = 0.54
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "AUS"
		}
		requested_factories = 1
		progress = 0.74
		efficiency = 100
	}
}
